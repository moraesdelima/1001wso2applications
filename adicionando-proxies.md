# Adicionando um Serviço Proxy para um Endpoint WSDL

Um serviço de proxy define serviços virtuais hospedados no ESB que podem aceitar solicitações, mediá-las e entregá-los a um serviço real. Um serviço de proxy pode executar mudanças de transporte ou interface e expor diferentes semânticas como WSDL, políticas e aspectos QoS como WS-Security do serviço real. Pode mediar as mensagens antes de serem entregues no ponto final real e mediar as respostas antes de chegarem ao cliente. Você também pode listar uma combinação de tarefas a serem executadas nas mensagens recebidas pelo serviço de proxy e encerrar o fluxo ou você pode enviar uma mensagem de volta para o cliente mesmo sem enviá-lo para o serviço real.

## Para adicionar um novo serviço de proxy para um Endpoint WSDL
1. Clique na guia Principal no Console de Gerenciamento. Vá para **Manage** -> **Services** -> **Add** e, em seguida, clique em **Proxy Service**.

    ![console_de_gerenciamento](http://ziben.com.br/coisitas/proxy-images/1_console_de_gerenciamento.jpg)

2. Selecione o template `Custom Proxy`, para iniciar o assistente de criação do serviço de proxy, onde você criará um novo serviço de proxy personalizado.

    ![selecao_template](http://ziben.com.br/coisitas/proxy-images/2_selecao_template.jpg)
    
3. No primeiro passo é preciso informar as configurações básicas do serviço e em siguida clicar em `Next`.
    
    |Parâmetro| Descrição | Tipo | Valor |  
    | ------ | ------ | ------ | ------ | 
    | Proxy Service name | Um nome exclusivo para o serviço de proxy. | Texto | BilheteWS |
    |  publishing a WSDL | Você pode publicar um WSDL para o serviço de proxy usando este atributo | Seleção | Pick from Endpoint |
    | Endpoint Key | Selecionando a opção `Pick from Endpoint` no atributo `publishing a WSDL` é possível carregar um documento WSDL a partir do registro, especificando este atributo | Seleção | Endpoint pré-existente |
    | Transport settings | Seleciona os transportes que o serviço proxy usará. Aqui, são exibidos apenas os transportes habilitados | Seleção | http, https |

    ![selecao_endpoint](http://ziben.com.br/coisitas/proxy-images/3_selecao_endpoint.jpg)
    
4. No segundo passo é possível informar o alvo do serviço de proxy pela adição de um `Endpoint` para serviço desejado. para criação do proxy não será necessário a definição de uma sequência de entrada, logo, após a definição do Endpoint, podemos seguir para o próximo passo clicando em `Next`

    ![define_endpoint](http://ziben.com.br/coisitas/proxy-images/4_define_endpoint.jpg)
    
5. No terceiro passo, caso necessário, podemos configure cabeçalhos HTTP personalizados na `Out Sequence` do serviço proxy para definir a propriedade "Access-Control-Allow-Origin" e permitir páginas de um front-end javascript / html seja capaz de obter o XML fornecido pelo serviço. Para isso selecione a opção `Define inline`e em seguida a opção `Create`. Se não houver necessidade pule para o passo 8

    ![define_endpoint](http://ziben.com.br/coisitas/proxy-images/5_enable_cors.jpg)

6. Em seguida, na tela de configuração da sequence adicione uma nova propriedade selecionando `Add Child` / `Core` / `Property`

    ![select_property_mediator](http://ziben.com.br/coisitas/proxy-images/6_select_property_mediator.jpg)

7. Preencha os seguintes atributos para a propriedade que esta sendo adicionada e depois grave as alterações clicando em `Save & Close`
    
    |Parâmetro| Descrição | Tipo | Valor |  
    | ------ | ------ | ------ | ------ | 
    | Name | Nome da propriedade | Texto | Access-Control-Allow-Origin |  
    | Action | A ação a ser realizada para a propriedade | Seleção | Set |  
    | Set Action as | indica se será utilizado um valor estático ou uma expressão | Seleção | Value |      
    | Type | O tipo de dados para a propriedade | Seleção | String |  
    | Value | NoValor da propriedade | Texto | * |       
    | Scope | O escopo no qual a propriedade será configurada | Seleção | Transport |    

    ![configure_property](http://ziben.com.br/coisitas/proxy-images/7_configure_property.jpg)
    
8. Para finalizar clique em `Finish` e o console exibirá a lista de serviços carregados no ESB.

    ![finish_proxy](http://ziben.com.br/coisitas/proxy-images/8_finish_proxy.jpg)

    ![finish_proxy](http://ziben.com.br/coisitas/proxy-images/9_deployed_services.jpg)

