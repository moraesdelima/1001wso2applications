# Adicionando um Endpoint

Um Endpoint define uma destino externo para envio de mensagem através do WSO2 Enterprise Integrator. Normalmente, o Endpoint é o endereço de um serviço de proxy, que atua como front end para o serviço real. 

## Para adicionar um novo Endpoint
1. Clique na guia Principal no Console de Gerenciamento. Vá para **Manage** -> **Service Bus** e, em seguida, clique em **Endpoints**.

	![console_de_gerenciamento](http://ziben.com.br/coisitas/endpoint-images/1_console_de_gerenciamento.jpg)

2. Selecione WSDL Endpoint. Com esse tipo de Endpoint podemos definir o endpoint para um service/port específico do wsdl.

	![selecao_template](http://ziben.com.br/coisitas/endpoint-images/2_selecao_template.jpg)

3. Digite valores para os seguintes propriedades do endpoint. 
	
    |Parâmetro| Descrição | Tipo | Valor |  
    | ------ | ------ | ------ | ------ | 
    | Name | Um nome exclusivo para o Endpoint. | Texto | BilheteWSSOAP12
    | WSDL URI | A URI para o WSDL. | Texto | http://sistemas.integrajca.com.br:8080/axis2/services/BilheteWS?wsdl |
    | Service | O "serviço" selecionado dentre os serviços disponíveis para o WSDL. | Texto | BilheteWS |
    | Port | a porta selecionada para o serviço especificado no campo acima. Em um WSDL, um endpoint pode ser vinculado a cada porta dentro de cada serviço.| Texto | BilheteWSSOAP12port_http |

4. Após preencher as propriedades do Endpoint, clique em **Test URI**

    ![dados_endpoint](http://ziben.com.br/coisitas/endpoint-images/3_dados_endpoint.jpg)
    
5. Para Finalizar clique em **Save & Close**. O Console de gerenciamento irá exibir a lista com os endpoints criados
6. 
    ![dados_endpoint](http://ziben.com.br/coisitas/endpoint-images/4_endpoint_criado.jpg)