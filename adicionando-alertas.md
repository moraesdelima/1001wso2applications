# Trabalhando com eventos no WSO2 Analytics e WSO2 Enterprise Integrator

Nesse projeto criaremos eventos no WSO2 Analytics para:

  - Definir alertas de indisponibilidade de serviços com proxy no WSO2 Integrator
  - Definir Gadgets que mostrem o tempo de resosta desses serviços

# Definir alertas de indisponibilidade de serviços com proxy no WSO2 Integrator Parte 1 - Analytics

## Pré requisitos
1. Afim de enviar alertas por email é necessário configurar um `sender` de email no Analytics. Para esse fim alteramos as propriedades `mail.smtp.from`, `mail.smtp.user` e `mail.smtp.password` no arquivo `<WSO2_HOME>/wso2/analytics/conf/output-event-adapters.xml`

```xml
<adapterConfig type="email">
    <!-- Comment mail.smtp.user and mail.smtp.password properties to support connecting SMTP servers which use trust
    based authentication rather username/password authentication -->
    <property key="mail.smtp.from">ws02.1001@gmail.com</property>
    <property key="mail.smtp.user">ws02.1001</property>
    <property key="mail.smtp.password">wso2.1001</property>
    <property key="mail.smtp.host">smtp.gmail.com</property>
    <property key="mail.smtp.port">587</property>
    <property key="mail.smtp.starttls.enable">true</property>
    <property key="mail.smtp.auth">true</property>
    <!-- Thread Pool Related Properties -->
    <property key="minThread">8</property>
    <property key="maxThread">100</property>
    <property key="keepAliveTimeInMillis">20000</property>
    <property key="jobQueueSize">10000</property>
</adapterConfig>
```
OBS.: A conta ws02.1001@gmail.com foi criada no gmail para fim de testes.

## Criando um novo evento no Analytics para geração de alertas de indisponibilidade
Utilizaremos como base para criação dos eventos o serviço [EscalaWS](http://sistemas.integrajca.com.br:8080/axis2/services/EscalaWS?wsdl) que ja possui um proxy configurado no Barramento através deste [WSDL](http://esb.integrajca.com.br:8280/services/EscalaWS?wsdl).

1. Faça Login no Console de gerenciamento do Analytics, acione a opçõa de menu Manage/Event/Streams e em seguida clique em `Add Event Stream`

    ![01-Analyitics-Manage-Event-Streams](./resources/Alerta/01-Analyitics-Manage-Event-Streams.jpg)

2. Para exemplificar a utilização dos eventos iremos criar um novo evento que receba informações sobre a indisponibilidade do serviço EscalaWS. Na janela `Define New Event Stream` Entre com as seguintes informações:
    - Event Stream Name: EscalaWSFaultStream
    - Event Strwam Version: 1.0.0

    ![02-New-Event-Stream.jpg](./resources/Alerta/02-New-Event-Stream.jpg)
    
 3. Em seguida role a tela para baixo, adicione os atributos definidos a seguir em `Payload Data Attributes` e acione o botão `Next [Persist Event]`
    - `filtroCPF`: String (Este atributo receberá o valor do atributo filtroCPF enviado para o serviço EscalaWS)
    - `filtroMatricula`: String (Este atributo receberá o valor do atributo filtroMatricula enviado para o serviço EscalaWS)
    - `filtroIdEmpresaGlobus`: int (Este atributo receberá o valor do atributo filtroIdEmpresaGlobus enviado para o serviço EscalaWS)
    - `tempoProcessamento`: long (Este atributo receberá o tempo de processamento)
    - `dataProcessamento`: String (Este atributo receberá a data de processamento)
    
    ![03-Payload-Data-Attributes.jpg](./resources/Alerta/03-Payload-Data-Attributes.jpg)

4. Marque a opção `Persist Event Stream`, selecione a opção `EVENT_STORE`, marque todos os atributos definidos anteriormente, certifique-se de que a coluna `Index Column` foi marcada para todos os atributos e acione o comando `Save Event Stream`
    ![04-Persist-Event-Stream.jpg](./resources/Alerta/04-Persist-Event-Stream.jpg)

5. O Console exibirá mensagem de sucesso da criação do evento
    ![05-Save-Event-Stream.jpg](./resources/Alerta/05-Save-Event-Stream.jpg)

## Criando um novo receptor para o evento criado
Para que um evento receba informações externas é necessário criar um receptor. Existem varios meios pelos quais podemos receber as informações de um evento. Em nosso exemplo configuraremos um endoint HTTP.

1. Para criar um novo receptor, acione a opçõa de menu Manage/Event/Receivers e em seguida clique em `Add Event Receiver`
    ![06-Analyitics-Manage-Event-Receivers.jpg](./resources/Alerta/06-Analyitics-Manage-Event-Receivers.jpg)

2. Na janela `Define New Event Stream` Entre com as informações definidas a seguir e acione o comando `Add Event Receiver`:
    - Event Receiver Name: EscalaWSFaultStreamReceiver
    - Input Event Adapter Type: http
    - Transport(s): all
    - Basic Auth Enable: true
    - Event Stream: EscalaWSFaultStream:1.0.0
    - Message Format: json

    ![07-New-Event-Receiver.jpg](./resources/Alerta/07-New-Event-Receiver.jpg)

3. O Console exibirá mensagem de sucesso da criação do receptor
    ![08-Add-Event-Receiver.jpg](./resources/Alerta/08-Add-Event-Receiver.jpg)

## Criando um novo alerta para o evento criado
Nosso objetivo é ser notificado por email sempre que o evento `EscalaWSFaultStream` for acionado. Para isso precisamos criar um novo publicador. De forma semelhante ao Receptor podemos publicar o acionamento de um evento de varias formas, porém para este projeto a forma escolhida foi publicar através de email.

1. Para criar um novo alerta de email, acione a opçõa de menu Manage/Event/Publishers e em seguida clique em `Add Event Publisher`
    ![09-Analyitics-Manage-Event-Publishers.jpg](./resources/Alerta/09-Analyitics-Manage-Event-Publishers.jpg)

2. Na janela `Define New Event Publisher` Entre com as informações definidas a seguir e acione o comando `Add Event Publisher`:
    - Event Publisher Name: EscalaWSFaultStreamPublisher
    - Event Source: EscalaWSFaultStream:1.0.0
    - Stream Atributes:
        - filtroCPF string, 
        - filtroMatricula string,
        - filtroIdEmpresaGlobus int,
        - tempoProcessamento long,
        - dataProcessamento string
    - Output Event Adapter: email
    - Email Address: Lista de destinatários separados por `,`
    - Subject: Assunto do email (Ex.: Falha no acesso ao serviço EscalaWS)
    - Email Type: text/plan
    - Message Format: text

    ![10-New-Event-Publisher.jpg](./resources/Alerta/10-New-Event-Publisher.jpg)

3. O Console exibirá mensagem de sucesso da criação do alerta
    ![11-Add-Event-Publisher.jpg](./resources/Alerta/11-Add-Event-Publisher.jpg)

	
# Definir alertas de indisponibilidade de serviços com proxy no WSO2 Integrator Parte 2 - Integrator

Existem diferentes modos de acionar um evento a partir do Integrator. Optamos por chamar a url do receptor do evento a partir da faultsequence do proxy, assim o evento EscalaWSFaultStream será chamado sempre que ocorrer um erro na chamada ao serviço externo.

## Criando uma sequencia de entrada para inicialiar as variaveis a serem enviadas ao evento
Criaremos uma sequencia única para inicialização das variáveis a seremenviadas aos eventos de falha e de controle de tempo de execução do EscalaWS. Chamaremmos essa sequence de `EscalaWSEventStreams.init`

1. Faça Login no Console de gerenciamento do Enterprise Integrator, acione a opçõa de menu Manage/Service Bus/Sequences e em seguida clique em `Add Sequence`
Copie e cole o conteúdo dentro da caixa de texto exibida para edição da sequence e em sguida acione o comando Save & C
    ![12-Integrator-Manage-Service-Bus-Sequences.jpg](./resources/Alerta/12-Integrator-Manage-Service-Bus-Sequences.jpg)

2. Em `Design Sequence` selecione a opção `switch to source view`.

    ![13-Design-Sequence-Init.jpg](./resources/Alerta/13-Design-Sequence-Init.jpg)

3. Copie e cole o conteúdo abaixo dentro da caixa de texto exibida para edição da sequence e em sguida acione o comando `Save & Close`.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sequence name="EscalaWSEventStreams.init" xmlns="http://ws.apache.org/ns/synapse">
    <property expression="get-property('SYSTEM_TIME')" name="dataInicio"
        scope="default" type="LONG"
        xmlns:ns="http://org.apache.synapse/xsd" xmlns:ns3="http://org.apache.synapse/xsd"/>
    <property expression="get-property('SYSTEM_DATE')"
        name="dataProcessamento" scope="default" type="STRING"
        xmlns:ns="http://org.apache.synapse/xsd" xmlns:ns3="http://org.apache.synapse/xsd"/>
    <filter xmlns:ns="http://org.apache.synapse/xsd"
        xmlns:ns3="http://org.apache.synapse/xsd"
        xmlns:xsd="http://ws/integrajca/com/br/xsd" xpath="//xsd:filtroIdEmpresaGlobus">
        <then>
            <property expression="//xsd:filtroIdEmpresaGlobus" name="filtroIdEmpresaGlobus"/>
        </then>
        <else>
            <property name="filtroIdEmpresaGlobus" value="0"/>
        </else>
    </filter>
    <filter xmlns:ns="http://org.apache.synapse/xsd"
        xmlns:ns3="http://org.apache.synapse/xsd"
        xmlns:xsd="http://ws/integrajca/com/br/xsd" xpath="//xsd:filtroMatricula">
        <then>
            <property expression="//xsd:filtroMatricula" name="filtroMatricula"/>
        </then>
        <else>
            <property name="filtroMatricula" value=""/>
        </else>
    </filter>
    <filter xmlns:ns="http://org.apache.synapse/xsd"
        xmlns:ns3="http://org.apache.synapse/xsd"
        xmlns:xsd="http://ws/integrajca/com/br/xsd" xpath="//xsd:filtroCPF">
        <then>
            <property expression="//xsd:filtroCPF" name="filtroCPF"/>
        </then>
        <else>
            <property name="filtroCPF" value=""/>
        </else>
    </filter>
</sequence>
```

![14-Init-Source.jpg](./resources/Alerta/14-Init-Source.jpg)

4. O Sistema retornará a exibição das sequencias criadas

    ![15-Save-Close.jpg](./resources/Alerta/15-Save-Close.jpg)

## Criando uma sequencia de falha para acionamento do evento de falha - EscalaWSFaultStream.

1. Dando sequencia ao passo anterior adicione uma nova sequencia e em seguida selecione a opção `switch to source view`.

    ![16-Design-Sequence-Send.jpg](./resources/Alerta/16-Design-Sequence-Send.jpg)

2. Copie e cole o conteúdo abaixo dentro da caixa de texto exibida para edição da sequence e em sguida acione o comando `Save & Close`.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sequence name="EscalaWSFaultStream.send" xmlns="http://ws.apache.org/ns/synapse">
	<clone continueParent="true" sequential="false">
		<target>
			<sequence>
				<property expression="get-property('SYSTEM_TIME')"
					name="dataFim" scope="default" type="LONG"
					xmlns:ns="http://org.apache.synapse/xsd" xmlns:ns3="http://org.apache.synapse/xsd"/>
				<script language="js">
				<![CDATA[
					var dataInicio = mc.getProperty("dataInicio");
					var dataFim= mc.getProperty("dataFim");
					mc.setProperty("tempoProcessamento",(dataFim-dataInicio).toString()  );
				]]>
				</script>
				<payloadFactory media-type="json">
					<format>
								{
									"event": {
										"payloadData": {
											"filtroCPF": "$ctx:filtroCPF",
											"filtroMatricula": "$ctx:filtroMatricula",
											"filtroIdEmpresaGlobus": $ctx:filtroIdEmpresaGlobus,
											"tempoProcessamento": $ctx:tempoProcessamento,
											"dataProcessamento": $ctx:dataProcessamento
										}
									}
								}
							</format>
				</payloadFactory>
				<property name="messageType" scope="axis2" type="STRING" value="application/json"/>
				<call>
					<endpoint>
						<http method="POST" uri-template="http://10.20.0.254:9764/endpoints/EscalaWSFaultStreamReceiver"/>
					</endpoint>
				</call>
				<drop/>
			</sequence>
		</target>
	</clone>
</sequence>
```
![17-Send-Source.jpg](./resources/Alerta/17-Send-Source.jpg)


    
## Alterando o proxy para acionar o evento EscalaWSFaultStream

Após a criação das sequencias de inicialização e envio, vamos alterar o proxy EscalaWS para acionar o evento EscalaWSFaultStream 

1. Selecione a opção `Manage/Services/List` do console de gerenciamento do Enterprise Integrator

    ![18-Integrator-Manage-Services-List.jpg](./resources/Alerta/18-Integrator-Manage-Services-List.jpg)

2. Pesquise pela palavra `Escala` e clique sobre o link `Source View` do proxy `EscalaWS`

    ![19-Find-EscalaWS.jpg](./resources/Alerta/19-Find-EscalaWS.jpg)

3. Adicione os trechos descritos abaixo (inSequence e faultSequence) conforme mostra a imagem e depois acione o comando `Save`

- inSequence
```xml
<inSequence>
    <sequence key="EscalaWSEventStreams.init"/>
</inSequence>
```

- faultSequence
```xml
<faultSequence>
    <sequence key="EscalaWSFaultStream.send"/>
    <property name="messageType" scope="axis2" type="STRING" value="text/xml"/>
    <send/>
</faultSequence>
```

![20-Edit-EscalaWS-Source.jpg](./resources/Alerta/20-Edit-EscalaWS-Source.jpg)






