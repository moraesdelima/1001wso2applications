# Trabalhando com eventos no WSO2 Analytics e WSO2 Enterprise Integrator

Nesse projeto criaremos eventos no WSO2 Analytics para:

  - Definir alertas de indisponibilidade de serviços com proxy no WSO2 Integrator
  - Definir Gadgets que mostrem o tempo de resosta desses serviços

# Definir Gadgets que mostrem o tempo de resposta de serviços com proxy no WSO2 Integrator Parte 1 - Analytics

## Criando um novo evento no Analytics para monitoramento do tempo de execução de um serviço
Utilizaremos como base para criação dos eventos o serviço [EscalaWS](http://sistemas.integrajca.com.br:8080/axis2/services/EscalaWS?wsdl) que ja possui um proxy configurado no Barramento através deste [WSDL](http://esb.integrajca.com.br:8280/services/EscalaWS?wsdl).

1. Faça Login no Console de gerenciamento do Analytics, acione a opçõa de menu Manage/Event/Streams e em seguida clique em `Add Event Stream`

    ![01-Analyitics-Manage-Event-Streams](./resources/Gadget/01-Analyitics-Manage-Event-Streams.jpg)
    
 2. Para exemplificar iremos criar um novo evento que receba informações sobre a o tempo de execução do serviço EscalaWS. Na janela `Define New Event Stream` Entre com as seguintes informações:
    - Event Stream Name: EscalaWSExecutionStream
    - Event Stream Version: 1.0.0

    ![02-New-Event-Stream.jpg](./resources/Gadget/02-New-Event-Stream.jpg)

 3. Em seguida role a tela para baixo, adicione os atributos definidos a seguir em `Payload Data Attributes` e acione o botão `Next [Persist Event]`
    - `filtroCPF`: String (Este atributo receberá o valor do atributo filtroCPF enviado para o serviço EscalaWS)
    - `filtroMatricula`: String (Este atributo receberá o valor do atributo filtroMatricula enviado para o serviço EscalaWS)
    - `filtroIdEmpresaGlobus`: int (Este atributo receberá o valor do atributo filtroIdEmpresaGlobus enviado para o serviço EscalaWS)
    - `tempoProcessamento`: long (Este atributo receberá o tempo de processamento)
    - `dataProcessamento`: String (Este atributo receberá a data de processamento)
    
    ![03-Payload-Data-Attributes.jpg](./resources/Gadget/03-Payload-Data-Attributes.jpg)

4. Marque a opção `Persist Event Stream`, selecione a opção `EVENT_STORE`, marque todos os atributos definidos anteriormente, certifique-se de que a coluna `Index Column` foi marcada para todos os atributos e acione o comando `Save Event Stream`
    ![04-Persist-Event-Stream.jpg](./resources/Gadget/04-Persist-Event-Stream.jpg)

5. O Console exibirá mensagem de sucesso da criação do evento
    ![05-Save-Event-Stream.jpg](./resources/Gadget/05-Save-Event-Stream.jpg)

## Criando um novo receptor para o evento criado
Para que um evento receba informações externas é necessário criar um receptor. Existem varios meios pelos quais podemos receber as informações de um evento. Em nosso exemplo configuraremos um endoint HTTP.

1. Para criar um novo receptor, acione a opçõa de menu Manage/Event/Receivers e em seguida clique em `Add Event Receiver`
    ![06-Analyitics-Manage-Event-Receivers.jpg](./resources/Gadget/06-Analyitics-Manage-Event-Receivers.jpg)

2. Na janela `Define New Event Stream` Entre com as informações definidas a seguir e acione o comando `Add Event Receiver`:
    - Event Receiver Name: EscalaWSExecutionStreamReceiver
    - Input Event Adapter Type: http
    - Transport(s): all
    - Basic Auth Enable: true
    - Event Stream: EscalaWSExecutionStream:1.0.0
    - Message Format: json

    ![07-New-Event-Receiver.jpg](./resources/Gadget/07-New-Event-Receiver.jpg)
    
3. O Console exibirá mensagem de sucesso da criação do receptor
    ![08-Add-Event-Receiver.jpg](./resources/Gadget/08-Add-Event-Receiver.jpg)

# Definir Gadgets que mostrem o tempo de resposta de serviços com proxy no WSO2 Integrator Parte 2 - Integrator

Existem diferentes modos de acionar um evento a partir do Integrator. Optamos por chamar a url do receptor do evento a partir da faultsequence do proxy, assim o evento EscalaWSFaultStream será chamado sempre que ocorrer um erro na chamada ao serviço externo.

## Criando uma sequencia de entrada para inicialiar as variaveis a serem enviadas ao evento
Criaremos uma sequencia única para inicialização das variáveis a seremenviadas aos eventos de falha e de controle de tempo de execução do EscalaWS. Chamaremmos essa sequence de `EscalaWSEventStreams.init`

1. Faça Login no Console de gerenciamento do Enterprise Integrator, acione a opçõa de menu Manage/Service Bus/Sequences e em seguida clique em `Add Sequence`
Copie e cole o conteúdo dentro da caixa de texto exibida para edição da sequence e em sguida acione o comando Save & C
    ![09-Integrator-Manage-Service-Bus-Sequences.jpg](./resources/Gadget/09-Integrator-Manage-Service-Bus-Sequences.jpg)

2. Em `Design Sequence` selecione a opção `switch to source view`.

    ![10-Design-Sequence-Init.jpg](./resources/Gadget/10-Design-Sequence-Init.jpg)

3. Copie e cole o conteúdo abaixo dentro da caixa de texto exibida para edição da sequence e em sguida acione o comando `Save & Close`.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sequence name="EscalaWSEventStreams.init" xmlns="http://ws.apache.org/ns/synapse">
    <property expression="get-property('SYSTEM_TIME')" name="dataInicio"
        scope="default" type="LONG"
        xmlns:ns="http://org.apache.synapse/xsd" xmlns:ns3="http://org.apache.synapse/xsd"/>
    <property expression="get-property('SYSTEM_DATE')"
        name="dataProcessamento" scope="default" type="STRING"
        xmlns:ns="http://org.apache.synapse/xsd" xmlns:ns3="http://org.apache.synapse/xsd"/>
    <filter xmlns:ns="http://org.apache.synapse/xsd"
        xmlns:ns3="http://org.apache.synapse/xsd"
        xmlns:xsd="http://ws/integrajca/com/br/xsd" xpath="//xsd:filtroIdEmpresaGlobus">
        <then>
            <property expression="//xsd:filtroIdEmpresaGlobus" name="filtroIdEmpresaGlobus"/>
        </then>
        <else>
            <property name="filtroIdEmpresaGlobus" value="0"/>
        </else>
    </filter>
    <filter xmlns:ns="http://org.apache.synapse/xsd"
        xmlns:ns3="http://org.apache.synapse/xsd"
        xmlns:xsd="http://ws/integrajca/com/br/xsd" xpath="//xsd:filtroMatricula">
        <then>
            <property expression="//xsd:filtroMatricula" name="filtroMatricula"/>
        </then>
        <else>
            <property name="filtroMatricula" value=""/>
        </else>
    </filter>
    <filter xmlns:ns="http://org.apache.synapse/xsd"
        xmlns:ns3="http://org.apache.synapse/xsd"
        xmlns:xsd="http://ws/integrajca/com/br/xsd" xpath="//xsd:filtroCPF">
        <then>
            <property expression="//xsd:filtroCPF" name="filtroCPF"/>
        </then>
        <else>
            <property name="filtroCPF" value=""/>
        </else>
    </filter>
</sequence>
```

![11-Init-Source.jpg](./resources/Gadget/11-Init-Source.jpg)

4. O Sistema retornará a exibição das sequencias criadas

    ![12-Save-Close.jpg](./resources/Gadget/12-Save-Close.jpg)
    
## Criando uma sequencia para acionamento do evento de controle do tempo de execução - EscalaWSExecutionStream.

1. Dando sequencia ao passo anterior adicione uma nova sequencia e em seguida selecione a opção `switch to source view`.

    ![13-Design-Sequence-Send.jpg](./resources/Gadget/13-Design-Sequence-Send.jpg)
    
2. Copie e cole o conteúdo abaixo dentro da caixa de texto exibida para edição da sequence e em sguida acione o comando `Save & Close`.
```xml
<?xml version="1.0" encoding="UTF-8"?>
<sequence name="EscalaWSExecutionStream.send" xmlns="http://ws.apache.org/ns/synapse">
	<clone continueParent="true" sequential="false">
		<target>
			<sequence>
				<property expression="get-property('SYSTEM_TIME')"
					name="dataFim" scope="default" type="LONG"
					xmlns:ns="http://org.apache.synapse/xsd" xmlns:ns3="http://org.apache.synapse/xsd"/>
				<script language="js">
				<![CDATA[
					var dataInicio = mc.getProperty("dataInicio");
					var dataFim= mc.getProperty("dataFim");
					mc.setProperty("tempoProcessamento",(dataFim-dataInicio).toString()  );
				]]>
				</script>
				<payloadFactory media-type="json">
					<format>
								{
									"event": {
										"payloadData": {
											"filtroCPF": "$ctx:filtroCPF",
											"filtroMatricula": "$ctx:filtroMatricula",
											"filtroIdEmpresaGlobus": $ctx:filtroIdEmpresaGlobus,
											"tempoProcessamento": $ctx:tempoProcessamento,
											"dataProcessamento": $ctx:dataProcessamento
										}
									}
								}
							</format>
				</payloadFactory>
				<property name="messageType" scope="axis2" type="STRING" value="application/json"/>
				<call>
					<endpoint>
						<http method="POST" uri-template="http://10.20.0.254:9764/endpoints/EscalaWSExecutionStreamReceiver"/>
					</endpoint>
				</call>
				<drop/>
			</sequence>
		</target>
	</clone>
</sequence>
```
![14-Send-Source.jpg](./resources/Gadget/14-Send-Source.jpg)

    
## Alterando o proxy para acionar o evento EscalaWSExecutionStream

Após a criação das sequencias de inicialização e envio, vamos alterar o proxy EscalaWS para acionar o evento EscalaWSFaultStream 

1. Selecione a opção `Manage/Services/List` do console de gerenciamento do Enterprise Integrator

    ![15-Integrator-Manage-Services-List.jpg](./resources/Gadget/15-Integrator-Manage-Services-List.jpg)

2. Pesquise pela palavra `Escala` e clique sobre o link `Source View` do proxy `EscalaWS`

    ![16-Find-EscalaWS.jpg](./resources/Gadget/16-Find-EscalaWS.jpg)

3. Adicione os trechos descritos abaixo (inSequence e outSequence) conforme mostra a imagem e depois acione o comando `Save`

- inSequence
```xml
<inSequence>
    <sequence key="EscalaWSEventStreams.init"/>
</inSequence>
```

- outSequence
```xml
<outSequence>
    <sequence key="EscalaWSExecutionStream.send"/>
    <property name="messageType" scope="axis2" type="STRING" value="text/xml"/>
    <send/>
</outSequence>
```

![17-Edit-EscalaWS-Source.jpg](./resources/Gadget/17-Edit-EscalaWS-Source.jpg)

# Definir Gadgets que mostrem o tempo de resosta de serviços com proxy no WSO2 Integrator Parte 3 - Gadget

Para criar novos gadgets é necessário conhecimento prévio das linguagens HTML5 e javascript. EM nosso exemplo utilizamos as bibliotecas Javascript jQuery e Chart.js para exibição de gráfico com o tempo de resposta do serviço. Neste documento destacamos alguns pontos importantes para a criação / execução do gadget. 

1. Para criação do novo gadget iremos utilizar três arquivos que podem ser baixados [aqui](https://bitbucket.org/moraesdelima/1001wso2applications/src/5356ad27ff6d/gadget/EscalaWS). Copie esses arquivos para a pasta `<WSO2_HOME>/wso2/analytics/repository/deployment/server/jaggeryapps/portal/store/carbon.super/fs/gadget/EscalaWS` do servidor onde o Analytics encontra-se em execução.

- [index.xml](https://bitbucket.org/moraesdelima/1001wso2applications/src/5356ad27ff6d0b76555dc0d426811761eb6e37bf/gadget/EscalaWS/index.xml?at=master&fileviewer=file-view-default) - Arquivo de definição do gadget. A estrutura básica do arquivo XML é mostrada a seguir. Para mais detalhes sobre como criar XMLs de gadgets, acesse [Primeiros passos: API de gadgets.*](https://developers.google.com/gadgets/docs/gs). 
```xml
<Module> <!-- Elemento raiz da estrutura XML -->
    <ModulePrefs></ModulePrefs> <!--Elemento de configuração do gadget -->
    <Content></Content> <!-- Contém os dados que precisam ser renderizados. Em nosso exemplo utilizamos um documento HTML -->
</Module>
```
- [gadget.json](https://bitbucket.org/moraesdelima/1001wso2applications/src/5356ad27ff6d0b76555dc0d426811761eb6e37bf/gadget/EscalaWS/gadget.json?at=master&fileviewer=file-view-default) - Arquivo de configuração usado especificamente pelo WSO2 
- [index.png](https://bitbucket.org/moraesdelima/1001wso2applications/src/5356ad27ff6d0b76555dc0d426811761eb6e37bf/gadget/EscalaWS/index.png?at=master&fileviewer=file-view-default) - Imagem utilizada para identificar o gadget para utilização nos Dashboards, definida na propriedade `thumbnail` no arquivo de configuração `gadget.json`. 

2. Em nosso arquivo de definição do gadget poemos destacar os seguintes trechos:

```javascript
//Consulta aos dados do evento EscalaWSExecutionStream, através da API Rest do Analytics
$.ajax({
    dataType: "json",
    url: "https://34.204.141.2:9444/analytics/tables/ESCALAWSEXECUTIONSTREAM", 
```

```javascript
//Refresh do gadget a cada dez segundos
setTimeout(function(){updateChart()}, 10000);
```

# Definir Gadgets que mostrem o tempo de resosta de serviços com proxy no WSO2 Integrator Parte 4 - Dashboard

O último passo será exibir o gadge criado em um `Dashboard`. EM nosso exemplo criamos um novo dashboard para exemplificar o uso do gadget, porém os mesmos passos podem ser realizados para adicionar o gadget em um dashboard existente.

1. Faça Login no Console de gerenciamento do Analytics, acione a opçõa de menu Dashboasrd/Analytics Dashboard. Certifique-se de que o seu navegador não esteja bloqueando novas janelas "Pop-Up"

    ![18-Analytics-Dashboard](./resources/Gadget/18-Analytics-Dashboard.jpg)

2. Entre com as informações de Login no Dashboard Server

    ![19-Login-Dashboard-Server](./resources/Gadget/19-Login-Dashboard-Server.jpg)

3. Selecione o comando `CREATE DASHBOARD`

    ![20-Dashboard-Server](./resources/Gadget/20-Dashboard-Server.jpg)

4. Informe um Nome e URL o Dashboard e clique em `Next`para prosseguir. O campo URL é preenchido automáticamente a partir do campo Nome.

    ![21-Create-Dashboard](./resources/Gadget/21-Create-Dashboard.jpg)

5. Na tela de configurações do dashboard escolha a opção `Single Column` para criação de uma nova página.

    ![22-Page-Layout](./resources/Gadget/22-Page-Layout.jpg)

6. No menu lateral selecione a oção `Gadgets` e emseguida selecione o gadget `EscalaWS`

    ![23-Gadget-EscalaWS](./resources/Gadget/23-Gadget-EscalaWS.jpg)

7. Arraste o gadget para dentro de um bloco na nova página

    ![24-Gadget-Design](./resources/Gadget/24-Gadget-Design.jpg)

8. Selecione a opção `VIEW` para visualizar o novo dashboard

    ![25-Gadget-View](./resources/Gadget/25-Gadget-View.jpg)
