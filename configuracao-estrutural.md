# Configuração da Arquitetura Estrutural no Sistema Operacional

Consiste mostrar a arquitura da solução, produtos, diretórios e scripts utilizados na configuração do WSO2 ESB e Analytics.

## Visão estrutural da Solução

A solução em sua versão inicial possui três servidores distintos, nomeados *Analytics*, *Master* e *Node1*. O `Node1` é o 
servidor que pode ser replicado para `auto scaling` e o Master controla as instâncias que entrarem no Balancer.

* IP interno Master: 10.20.0.11 - IP externo: 35.153.174.165
* IP interno Node1: 10.20.0.17 - IP externo: 54.165.27.48
* IP interno Analytics: 10.20.0.254 - IP externo: 34.204.141.2

## Localização dos Servers ESB e Analytics

* Localização do servidor Analytics 
```shell
/opt/wso2-esb-cluster/wso2-alone/wso2ei-6.1.1/wso2/analytics/bin
```
* Para start / stop use o comando no local abaixo:
```shell
./sh wso2server.sh start
```

* Localização dos servidores ESB Master e todos os Nodes
```shell
/opt/wso2-esb-cluster/wso2-alone/wso2ei-6.1.1/bin
```

* Para start / stop use o comando abaixo
```shell
./sh integrator.sh start
```

## Processo de Sincronização

Para cada novo `node` criado para *auto scaling* deve ser acrescentado numa lista da máquina *Master* o IP da nova máquina que entra
assim como deve ser feito também no *Balancer*. O *script* e a lista estão em:
```
/opt/wso2-esb-cluster/wso2-sync
```

A lista de IPs está no arquivo `nodes-list.txt` que é executado pelo *script* `rsync-for-ei-depsync.sh` via `crontab` a cada 1 minuto, ou seja, toda vez que for criado
um novo serviço na *Master* em no máximo 1 minuto todos os *nodes* estarão atualizados, segue detalhes da lista:
```
ec2-user@10.20.0.17:/opt/wso2-esb-cluster/wso2-alone/wso2ei-6.1.1/repository/deployment/server
```

O *script* usa SSH e `rsync` para fazer a sincronização, veja abaixo:
```shell
#!/bin/sh
# reengineering by Claudio Cardozo at 04 march, 2018

ei_server_dir=/opt/wso2-esb-cluster/wso2-alone/wso2ei-6.1.1/repository/deployment/server/
pem_file=/home/ec2-user/.ssh/desenvolvimento.pem
   
# delete the lock on exit
trap 'rm -rf /opt/wso2-esb-cluster/wso2-sync/depsync-lock' EXIT

# dont create if exists  
mkdir -p /tmp/carbon-rsync-logs/
  
# keep a lock to stop parallel runs
if mkdir /opt/wso2-esb-cluster/wso2-sync/depsync-lock; then
  echo "Locking succeeded" >&2
else
  echo "Lock failed - exit" >&2
  exit 1
fi
    
# get the nodes-list.txt
pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd`
popd > /dev/null
echo $SCRIPTPATH
  
for x in `cat ${SCRIPTPATH}/nodes-list.txt`
do
    echo ================================================== >> /tmp/carbon-rsync-logs/esb-rsync.log;
    echo Syncing $x;
    rsync --delete -arve "ssh -i  $pem_file -o StrictHostKeyChecking=no" $ei_server_dir $x >> /tmp/carbon-rsync-logs/esb-rsync.log
	echo ================================================== >> /tmp/carbon-rsync-logs/esb-rsync.log;
done
```
Se olharmos a tabela de *crontab* ela está assim:
```shell
# sincroniza os nodes do esb a partir desta master
*  *  *  *  *   sudo /opt/wso2-esb-cluster/wso2-sync/rsync-for-ei-depsync.sh
```

* Antes de rodar um server qualquer, verificar se já não tem uma cópia rodando.


